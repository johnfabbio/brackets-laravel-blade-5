[<img src="http://img.shields.io/badge/version-0.3.1-green.svg?style=flat-square">](#)
[<img src="http://img.shields.io/badge/Code-Highlight-yellow.svg?style=flat-square">](#)
[<img src="http://img.shields.io/badge/Code-Hints-orange.svg?style=flat-square">](http://laravel.com/)
[<img src="http://img.shields.io/badge/Laravel-Blade-red.svg?style=flat-square">](http://laravel.com/)
[<img src="http://img.shields.io/badge/Brackets-code editor-blue.svg?style=flat-square">](http://brackets.io/)

Laravel Blade
=============
Resaltando de sintaxis y Sugerencias de código para [*Brackets code editor*](http://brackets.io/).

##Soporta:
*** AHORA Apoyo Blade Código Hints!  ***
Resaltado de sintaxis para:

*  **{{{---  Este es un comentario   ---}}}**
*  **{{  Laravel 4  }}**
*  **{%  Laravel 5  %}**
*  **{!! Laravel 5 !!}**

##Ahora:
=============
Resaltando nueva sintaxis, acutalizado con la version 5.5 de laravel.

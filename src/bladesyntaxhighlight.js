define(function (require, exports, module) {
	'use strict';

	var LanguageManager = brackets.getModule("language/LanguageManager");
	var CodeMirror 		= brackets.getModule("thirdparty/CodeMirror2/lib/codemirror");
    	var highlightClass  = "keyword";

	CodeMirror.defineMode("laravelblade", function (config, parserConfig) {

		var laravelOverlay = {
			startState: function() {
				return {
							inComment: false,
                    		inEcho: false,
                    		inEcho5: false,
                			inForm: false,
                			inInclude: false,
                			inSection: false,
                			inEndsection: false,
                			inShow: false,
                			inYield: false,
                			inExtends: false,

                			inPhp: false,
                			inJson: false,
                			inEach: false,

                			inAuth: false,
                			inEndauth: false,
                			inGuest: false,
                			inEndguest: false,

                			inIf: false,
                			inElseif: false,
                			inElse_if: false,
                			inElse: false,
                			inEndif: false,
                			inEndif: false,
                			inUnless: false,
                			inEndunless: false,


                			//LOOPS
                			inFor: false,
                			inForelse: false,
                			inForeach: false,
                			inEndfor: false,

                			inContinue: false,
                			inEndforeach: false,
                			inEmpty: false,
                			inEndempty: false,
                			inEndforelse: false,

                			inWhile: false,
                			inEndwhile: false,

                			inComponent: false,
                			inEndcomponent: false,
                			inSlot: false,
                			inEndslot: false,

                			inVerbatim: false,
                			inEndverbatim: false,

                			inIsset: false,
                			inEndisset: false,

                			inPush: false,
                			inEndpush: false,
                			inStack: false,
                			inInject: false,

                			inEnv: false,
                			inEndenv: false


				}
			},
			token: function(stream, state) {
				var ch;


				if (state.inComment) {
					if (!stream.skipTo("--}}")) {
						stream.skipToEnd();
					} else {
						stream.match("--}}");
						state.inComment = false;
					}
					return "comment";
				} else {
					if (stream.match("{{--")) {
						state.inComment = true;
						if (!stream.skipTo("--}}")) {
							stream.skipToEnd();
						} else {
							stream.match("--}}");
							state.inComment = false;
						}
						return "comment";
					}
				}


				if (state.inEcho5) {
					if (!stream.skipTo("%}")) {
						stream.skipToEnd();
					} else {
						stream.match("%}");
						state.inEcho5 = false;
					}
					return highlightClass;
				} else {
					if (stream.match("{%")) {
						state.inEcho5 = true;
						if (!stream.skipTo("%}")) {
							stream.skipToEnd();
						} else {
							stream.match("%}");
							state.inEcho5 = false;
						}
						return highlightClass;
					}
				}


				if (state.inEcho) {
					if (!stream.skipTo("}}")) {
						stream.skipToEnd();
					} else {
						stream.match("}}");
						state.inEcho = false;
					}
					return highlightClass;
				} else {
					if (stream.match("{{")) {
						state.inEcho = true;
						if (!stream.skipTo("}}")) {
							stream.skipToEnd();
						} else {
							stream.match("}}");
							state.inEcho = false;
						}
						return highlightClass;
					}
				}

				if (state.inPhp) {
					if (!stream.skipTo("@endphp")) {
						stream.skipToEnd();
					} else {
						stream.match("@endphp");
						state.inPhp = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@php")) {
						state.inPhp = true;
						if (!stream.skipTo("@endphp")) {
							stream.skipToEnd();
						} else {
							stream.match("@endphp");
							state.inPhp = false;
						}
						return highlightClass;
					}
				}

				if (state.inForm) {
					if (!stream.skipTo("!!}")) {
						stream.skipToEnd();
					} else {
						stream.match("!!}");
						state.inForm = false;
					}
					return highlightClass;
				} else {
					if (stream.match("{!!")) {
						state.inForm = true;
						if (!stream.skipTo("!!}")) {
							stream.skipToEnd();
						} else {
							stream.match("!!}");
							state.inForm = false;
						}
						return highlightClass;
					}
				}

				//CONDICIONAL IF -----------------------------------------------------------------
				if (state.inIf) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inIf = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@if")) {
						state.inIf = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inIf = false;
						}
						return highlightClass;
					}
				}

				if (state.inElseif) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inElseif = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@elseif")) {
						state.inElseif = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inElseif = false;
						}
						return highlightClass;
					}

				}
				if (state.inElse_if) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inElse_if = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@else if")) {
						state.inElse_if = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inElse_if = false;
						}
						return highlightClass;
					}

				}

				if (state.inElse) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match(" ");
						state.inElse = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@else")) {
						state.inElse = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inElse = false;
						}
						return highlightClass;
					}

				}
				if (state.inEndif) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndif = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endif")) {
						state.inEndif = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndif = false;
						}
						return highlightClass;
					}

				}

				if (state.inUnless) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match(" ");
						state.inUnless = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@unless")) {
						state.inUnless = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inUnless = false;
						}
						return highlightClass;
					}

				}
				if (state.inEndunless) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndunless = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endunless")) {
						state.inEndunless = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndunless = false;
						}
						return highlightClass;
					}

				}

				//FIN DE CONDICIONAL IF -----------------------------------------------------------------

				//CONDICIONALES LOOPS -----------------------------------------------------

				if (state.inForelse) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inForelse = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@forelse")) {
						state.inForelse = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inForelse = false;
						}
						return highlightClass;
					}
				}
				if (state.inForeach) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inForeach = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@foreach")) {
						state.inForeach = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inForeach = false;
						}
						return highlightClass;
					}
				}

				if (state.inFor) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inFor = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@for")) {
						state.inFor = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inFor = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndfor) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndfor = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endfor")) {
						state.inEndfor = true;
						if (!stream.skipTo(" ")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndfor = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndforeach) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndforeach = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endforeach")) {
						state.inEndforeach = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndforeach = false;
						}
						return highlightClass;
					}
				}

				if (state.inEmpty) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEmpty = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@empty")) {
						state.inEmpty = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEmpty = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndempty) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndempty = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endempty")) {
						state.inEndempty = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndempty = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndforelse) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndforelse = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endforelse")) {
						state.inEndforelse = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndforelse = false;
						}
						return highlightClass;
					}
				}



				             //while
				 if (state.inWhile) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inWhile = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@while")) {
						state.inWhile = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inWhile = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndwhile) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndwhile = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endwhile")) {
						state.inEndwhile = true;
						if (!stream.skipTo(") ")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndwhile = false;
						}
						return highlightClass;
					}
				}
				if (state.inContinue) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inContinue = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@continue")) {
						state.inContinue = true;
						if (!stream.skipTo(") ")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inContinue = false;
						}
						return highlightClass;
					}
				}


				// FIN CONDICIONALES LOOPS -------------------------------------------------------

				 //FUNCIONES
				if (state.inInclude) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inInclude = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@include")) {
						state.inInclude = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inInclude = false;
						}
						return highlightClass;
					}
				}

				if (state.inSection) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inSection = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@section")) {
						state.inSection = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inSection = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndsection) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndsection = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endsection")) {
						state.inEndsection = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndsection = false;
						}
						return highlightClass;
					}
				}

				if (state.inShow) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inShow = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@show")) {
						state.inShow = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inShow = false;
						}
						return highlightClass;
					}
				}

				if (state.inYield) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inYield = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@yield")) {
						state.inYield = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inYield = false;
						}
						return highlightClass;
					}
				}

				if (state.inExtends) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inExtends = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@extends")) {
						state.inExtends = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inExtends = false;
						}
						return highlightClass;
					}
				}

				if (state.inComponent) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inComponent = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@Component")) {
						state.inComponent = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inComponent = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndcomponent) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndcomponent = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endcomponent")) {
						state.inEndcomponent = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndcomponent = false;
						}
						return highlightClass;
					}
				}

				if (state.inSlot) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inSlot = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@slot")) {
						state.inSlot = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inSlot = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndslot) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndslot = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endslot")) {
						state.inEndslot = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndslot = false;
						}
						return highlightClass;
					}
				}
				if (state.inJson) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inJson = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@json")) {
						state.inJson = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inJson = false;
						}
						return highlightClass;
					}
				}

				if (state.inVerbatim) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inVerbatim = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@verbatim")) {
						state.inVerbatim = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inVerbatim = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndverbatim) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndverbatim = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endverbatim")) {
						state.inEndverbatim = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndverbatim = false;
						}
						return highlightClass;
					}
				}

				if (state.inIsset) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inIsset = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@isset")) {
						state.inIsset = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inIsset = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndisset) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndisset = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endisset")) {
						state.inEndisset = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndisset = false;
						}
						return highlightClass;
					}
				}

				if (state.inAuth) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inAuth = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@auth")) {
						state.inAuth = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inAuth = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndauth) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndauth = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endauth")) {
						state.inEndauth = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndauth = false;
						}
						return highlightClass;
					}
				}

				if (state.inGuest) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inGuest = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@guest")) {
						state.inGuest = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inGuest = false;
						}
						return highlightClass;
					}
				}

				if (state.inEndguest) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndguest = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endguest")) {
						state.inEndguest = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndguest = false;
						}
						return highlightClass;
					}
				}
				if (state.inEach) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEach = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@each")) {
						state.inEach = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEach = false;
						}
						return highlightClass;
					}
				}
				if (state.inPush) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inPush = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@push")) {
						state.inPush = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inPush = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndpush) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndpush = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endpush")) {
						state.inEndpush = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndpush = false;
						}
						return highlightClass;
					}
				}
				if (state.inStack) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inStack = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@stack")) {
						state.inStack = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inStack = false;
						}
						return highlightClass;
					}
				}
				if (state.inInject) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inInject = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@inject")) {
						state.inInject = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inInject = false;
						}
						return highlightClass;
					}
				}
				if (state.inEnv) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEnv = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@env")) {
						state.inEnv = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEnv = false;
						}
						return highlightClass;
					}
				}
				if (state.inEndenv) {
					if (!stream.skipTo("")) {
						stream.skipToEnd();
					} else {
						stream.match("");
						state.inEndenv = false;
					}
					return highlightClass;
				} else {
					if (stream.match("@endenv")) {
						state.inEndenv = true;
						if (!stream.skipTo("")) {
							stream.skipToEnd();
						} else {
							stream.match("");
							state.inEndenv = false;
						}
						return highlightClass;
					}
				}












                		//listen for opening tags
				while (stream.next() != null &&
					   !stream.match("{{--", false) &&
					   !stream.match("{%", false) &&
					   !stream.match("{!!", false) &&
					   !stream.match("{{", false)&&
					   !stream.match("@php", false)&&
					   !stream.match("@if", false)&&
					   !stream.match("@elseif", false)&&
					   !stream.match("@else if", false)&&
					   !stream.match("@else", false)&&
					   !stream.match("@endif", false)&&
					   !stream.match("@for", false)&&
					   !stream.match("@forelse", false)&&
					   !stream.match("@endfor", false)&&
					   !stream.match("@endforeach", false)&&
					   !stream.match("@endforelse", false)&&
					   !stream.match("@endwhile", false)&&
					   !stream.match("@empty", false)&&
					   !stream.match("@include", false)&&
					   !stream.match("@show", false)&&
					   !stream.match("@unless", false)&&
					   !stream.match("@endunless", false)&&
					   !stream.match("@yield", false)&&
					   !stream.match("@component", false)&&
					   !stream.match("@endcomponent", false)&&
					   !stream.match("@slot", false)&&
					   !stream.match("@endslot", false)&&
					   !stream.match("@json", false)&&
					   !stream.match("@verbatim", false)&&
					   !stream.match("@endverbatim", false)&&
					   !stream.match("@endempty", false)&&
					   !stream.match("@isset", false)&&
					   !stream.match("@endisset", false)&&
					   !stream.match("@auth", false)&&
					   !stream.match("@endauth", false)&&
					   !stream.match("@guest", false)&&
					   !stream.match("@endguest", false)&&
					   !stream.match("@each", false)&&
					   !stream.match("@push", false)&&
					   !stream.match("@endpush", false)&&
					   !stream.match("@stack", false)&&
					   !stream.match("@inject", false)&&
					   !stream.match("@env", false)&&
					   !stream.match("@endenv", false)
					  ) {}


					return null;


			}
		  };
		  return CodeMirror.overlayMode(CodeMirror.getMode(config, parserConfig.backdrop || "php"), laravelOverlay);
	});

	LanguageManager.defineLanguage("laravelblade", {
		"name": "Laravel Blade",
		"mode": "laravelblade",
		"fileExtensions": ["blade.php"],
		"blockComment": ["{{--", "--}}"]
	});
});
